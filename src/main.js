import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import Vuetify from "vuetify";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

new Vue({
  Vuetify,
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
