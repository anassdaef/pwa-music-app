/* eslint-disable no-console */

import { register } from "register-service-worker";

if (process.env.NODE_ENV === "production") {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready() {
      console.log(
        "App is being served from cache by a service worker.\n" +
          "For more details, visit https://goo.gl/AFskqB"
      );
    },
    registered() {
      console.log("Service worker has been registered.");
      let cacheName = "image_cache";
      let contentToCache = [
        "./images/linn da quebrada - menorme.jpg",
        "./images/mc tha - oceano.jpg",
        "./images/miley cyrus - fu.jpg",
        "./images/viktoria modesta - counterflow.jpg",
        "./index.html"
      ];
      caches.open(cacheName).then(cache => {
        console.log("[Service Worker] Caching all");
        return cache.addAll(contentToCache);
      });
    },
    cached() {
      console.log("Content has been cached for offline use.");
    },
    updatefound() {
      console.log("New content is downloading.");
    },
    updated() {
      console.log("New content is available; please refresh.");
    },
    offline() {
      console.log(
        "No internet connection found. App is running in offline mode."
      );
    },
    error(error) {
      console.error("Error during service worker registration:", error);
    }
  });
}
