import Vue from "vue";
import VueRouter from "vue-router";
import Vuetify from "vuetify";
import Player from "../components/Player.vue";

Vue.use(VueRouter);
Vue.use(Vuetify);

const routes = [
  {
    path: "/",
    name: "Player",
    component: Player
  },
  {
    path: "/playermusicinfo/:artist",
    name: "PlayerMusicInfo",
    component: () => import("../components/PlayerMusicInfo")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
