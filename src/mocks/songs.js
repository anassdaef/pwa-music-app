const songs = [
  {
    title: "MEnorme",
    artist: "Linn da Quebrada",
    spotify:
      "https://open.spotify.com/artist/5gGBopc7iw8yLqwxfPIv3t?autoplay=true",
    youtube: "https://www.youtube.com/channel/UCje0RwqumaW8Be1c1YKL7DA",
    country: "Brazil",
    bio:
      "Linn da Quebrada née Linna Pereira à São Paulo le 18 juillet 1990 est une actrice, chanteuse et compositrice brésilienne de funk et de pop. Elle est également une activiste pour les droits civils de la communauté LGBT et de la population noire",
    seconds: 244,
    cover: require("../../public/images/linn da quebrada - menorme.jpg"),
    src: require("../../public/audios/Linn da Quebrada - MEnorme.mp3"),
    isFavourite: true
  },
  {
    title: "Oceano",
    artist: "Mc Tha",
    spotify:
      "https://open.spotify.com/artist/5gGBopc7iw8yLqwxfPIv3t?autoplay=true",
    youtube: "https://www.youtube.com/channel/UCSnVLlL_pFn1MJ3LvZxWhjg",
    country: "Brazil",
    bio:
      "Linn da Quebrada née Linna Pereira à São Paulo le 18 juillet 1990 est une actrice, chanteuse et compositrice brésilienne de funk et de pop. Elle est également une activiste pour les droits civils de la communauté LGBT et de la population noire",
    seconds: 128,
    cover: require("../../public/images/mc tha - oceano.jpg"),
    src: require("../../public/audios/Mc Tha - Oceano.mp3"),
    isFavourite: false
  },
  {
    title: "FU",
    artist: "Miley Cyrus",
    spotify:
      "https://open.spotify.com/artist/5gGBopc7iw8yLqwxfPIv3t?autoplay=true",
    youtube: "https://www.youtube.com/channel/UCSnVLlL_pFn1MJ3LvZxWhjg",
    country: "Brazil",
    bio:
      "Linn da Quebrada née Linna Pereira à São Paulo le 18 juillet 1990 est une actrice, chanteuse et compositrice brésilienne de funk et de pop. Elle est également une activiste pour les droits civils de la communauté LGBT et de la population noire",
    seconds: 229,
    cover: require("../../public/images/miley cyrus - fu.jpg"),
    src: require("../../public//audios/Miley Cyrus - FU.mp3"),
    isFavourite: false
  },
  {
    title: "Counterflow",
    artist: "Viktoria Modesta",
    spotify:
      "https://open.spotify.com/artist/5gGBopc7iw8yLqwxfPIv3t?autoplay=true",
    youtube: "https://www.youtube.com/channel/UCSnVLlL_pFn1MJ3LvZxWhjg",
    country: "Brazil",
    bio:
      "Linn da Quebrada née Linna Pereira à São Paulo le 18 juillet 1990 est une actrice, chanteuse et compositrice brésilienne de funk et de pop. Elle est également une activiste pour les droits civils de la communauté LGBT et de la population noire",
    seconds: 204,
    cover: require("../../public/images/viktoria modesta - counterflow.jpg"),
    src: require("../../public/audios/Viktoria Modesta - Counterflow.mp3"),
    isFavourite: false
  }
];

export default songs;
